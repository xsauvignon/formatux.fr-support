#!/bin/bash

# depends : Asciidoctor-pdf
# Installation
# gem install --pre asciidoctor-pdf
# Ajouter le PATH dans votre $HOME/.bashrc :
# export PATH="$HOME/.gem/ruby/2.4.0/bin:$PATH"
# $1 = chemin vers le chemin de build
DIRNAME=$(dirname $0)
SOURCES=$DIRNAME/../sources/
DEST=$DIRNAME/../
DATE=$(date +%x) # La date au format Local

# Test si un argument est fourni
if [ $# -eq 0 ]
  then
    echo "Aucun argument fourni : fournir un fichier adoc svp"
    exit 1
fi

# Generation des fichiers PDF
  echo "Debut de la compilation du fichier $1"
  asciidoctor-pdf                            \
    -a pdf-stylesdir="${SOURCES}/theme/"     \
    -t                                       \
    -n                                       \
    -a pdf-style="asciidoctor"               \
    -a pdf-fontsdir="${SOURCES}/theme/fonts" \
    -a lang="fr"                             \
    -a icons="font"                          \
    -a chapter-label="Chapitre"              \
    -a encoding="utf-8"                      \
    -a toc="preamble"                        \
    -a toc-title="Table des matières"        \
    -a part-title="Partie "                  \
    -a toclevels=3                           \
    -a numbered                              \
    -a sectnumlevels=2                       \
    -a showtitle="titre"                     \
    -a experimental                          \
    -D $DEST $1
  echo "Fin de la compilation du fichier $1"
  echo "----------------------------------------"
  echo ""
